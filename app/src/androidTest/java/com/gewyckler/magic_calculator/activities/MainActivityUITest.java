package com.gewyckler.magic_calculator.activities;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.Intents.intending;
import static androidx.test.espresso.intent.matcher.IntentMatchers.toPackage;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;

import android.Manifest;
import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import androidx.test.espresso.intent.Intents;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.GrantPermissionRule;

import com.example.magic_calculator.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(AndroidJUnit4ClassRunner.class)
public class MainActivityUITest {

    private final String DEFAULT_VALUE = "0";

    @Rule
    public ActivityScenarioRule<MainActivity> activityScenarioRule = new ActivityScenarioRule<>(MainActivity.class);

    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule.grant(Manifest.permission.READ_CONTACTS);

    @Before
    public void setUp() {
        Intents.init();
    }

    @After
    public void tearDown() {
        Intents.release();
    }

    @Test
    public void test_isActivityInView() {
        onView(withId(R.id.main_layout)).check(matches(isDisplayed()));
    }

    @Test
    public void test_visibility_resultScreen_allButtons() {
        onView(withId(R.id.result_screen)).check(matches(isDisplayed()));
        onView(withId(R.id.history_clear_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.back_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.clear_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.open_brackets_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.close_brackets_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.power_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.sum_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.one_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.two_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.three_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.subtraction_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.four_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.five_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.six_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.multiply_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.seven_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.eight_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.nine_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.divide_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.plus_minus_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.zero_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.comma_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.result_btn)).check(matches(isDisplayed()));
    }

    @Test
    public void test_isDefaultTextDisplayedOnResultScreen() {
        onView(withId(R.id.result_screen)).check(matches(withText(R.string.default_screen)));
    }

    @Test
    public void test_displayZeroOnResultScreen() {
        onView(withId(R.id.zero_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(R.string.zero)));
    }

    @Test
    public void test_displayOneOnResultScreen() {
        onView(withId(R.id.one_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(R.string.one)));
    }

    @Test
    public void test_displayTwoOnResultScreen() {
        onView(withId(R.id.two_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(R.string.two)));
    }

    @Test
    public void test_displayThreeOnResultScreen() {
        onView(withId(R.id.three_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(R.string.three)));
    }

    @Test
    public void test_displayFourOnResultScreen() {
        onView(withId(R.id.four_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(R.string.four)));
    }

    @Test
    public void test_displayFiveOnResultScreen() {
        onView(withId(R.id.five_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(R.string.five)));
    }

    @Test
    public void test_displaySixOnResultScreen() {
        onView(withId(R.id.six_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(R.string.six)));
    }

    @Test
    public void test_displaySevenOnResultScreen() {
        onView(withId(R.id.seven_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(R.string.seven)));
    }

    @Test
    public void test_displayEightOnResultScreen() {
        onView(withId(R.id.eight_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(R.string.eight)));
    }

    @Test
    public void test_displayNineOnResultScreen() {
        onView(withId(R.id.nine_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(R.string.nine)));
    }

    @Test
    public void test_displayOpenBracketOnResultScreen() {
        onView(withId(R.id.open_brackets_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(R.string.open_brackets)));
    }

    @Test
    public void test_displayCloseBracketOnResultScreen() {
        onView(withId(R.id.close_brackets_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(R.string.close_brackets)));
    }

    @Test
    public void test_displayPowerOnResultScreen() {
        String expected = DEFAULT_VALUE + "^";
        onView(withId(R.id.power_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(expected)));
    }

    @Test
    public void test_displayPlusOnResultScreen() {
        String expected = DEFAULT_VALUE + "+";
        onView(withId(R.id.sum_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(expected)));
    }

    @Test
    public void test_displayMinusOnResultScreen() {
        String expected = DEFAULT_VALUE + "-";
        onView(withId(R.id.subtraction_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(expected)));
    }

    @Test
    public void test_displayMultiplyOnResultScreen() {
        String expected = DEFAULT_VALUE + "×";
        onView(withId(R.id.multiply_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(expected)));
    }

    @Test
    public void test_displayDivideOnResultScreen() {
        String expected = DEFAULT_VALUE + "÷";
        onView(withId(R.id.divide_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(expected)));
    }

    @Test
    public void test_displayPlusMinusOnResultScreen() {
        String expected = DEFAULT_VALUE + "-";
        onView(withId(R.id.plus_minus_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(expected)));
    }

    @Test
    public void test_displayCommaOnResultScreen() {
        String expected = DEFAULT_VALUE + ".";
        onView(withId(R.id.comma_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(expected)));
    }

    @Test
    public void test_typingTwoMathSymbolNextToEachOther() {
        String expectedOutput = "4+";
        onView(withId(R.id.four_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.multiply_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.sum_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(expectedOutput)));
    }

    @Test
    public void test_backSpaceBtn_notDeletingLastChar() {
        String expectedOutput = "2+3";
        onView(withId(R.id.two_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.sum_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.three_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.multiply_btn)).perform(closeSoftKeyboard(), click());

        onView(withId(R.id.back_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(expectedOutput)));
    }

    @Test
    public void test_backSpaceBtn_nothingToDelete() {
        onView(withId(R.id.back_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(R.string.default_screen)));
    }

    @Test
    public void test_backSpaceBtn_deletingLastChar() {
        onView(withId(R.id.two_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.back_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(R.string.default_screen)));
    }

    @Test
    public void test_resultBtn_giveCorrectResult() {
        String expectedOutput = "5.0";
        onView(withId(R.id.two_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.sum_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.three_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(expectedOutput)));
    }

    @Test
    public void test_resultBtn_giveNanError() {
        onView(withId(R.id.two_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.sum_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(R.string.error_message)));
    }

    @Test
    public void test_resultBtnLongClick_causePhoneIntent() {
        stubIntent();
        onView(withId(R.id.result_btn)).perform(closeSoftKeyboard(), longClick());
        intended(toPackage("com.android.contacts"));
    }

    @Test
    public void test_resultBtn_mustUpdateHistoryWithUserExpression() {
        String expectedOutput = "2\n";
        onView(withId(R.id.two_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.history_screen)).check(matches(withText(expectedOutput)));
    }

    @Test
    public void test_validPhoneIntent_checkPickedNumberWithResultScreen() {
        stubIntent();
        onView(withId(R.id.result_btn)).perform(closeSoftKeyboard(), longClick());

        Context targetContext2 = InstrumentationRegistry.getInstrumentation().getTargetContext();

        String number = getPhoneNumber(targetContext2);

        onView(withId(R.id.result_screen)).check(matches(withText(number)));
    }

    @Test
    public void test_clearBtn_clearResultScreen() {
        String input = "4*8/9";
        onView(withId(R.id.result_screen)).perform(typeText(input));
        onView(withId(R.id.clear_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_screen)).check(matches(withText(R.string.default_screen)));
    }

    @Test
    public void test_clearBtn_clearingPhoneNumber() {
        stubIntent();
        onView(withId(R.id.result_btn)).perform(closeSoftKeyboard(), longClick());
        Context targetContext2 = InstrumentationRegistry.getInstrumentation().getTargetContext();

        String number = getPhoneNumber(targetContext2);

        onView(withId(R.id.result_screen)).check(matches(withText(number)));

        onView(withId(R.id.clear_btn)).perform(closeSoftKeyboard(), longClick());
        onView(withId(R.id.result_btn)).perform(closeSoftKeyboard(), click());

        onView(withId(R.id.result_screen)).check(matches(withText("0.0")));
    }

    @Test
    public void test_clearBtn_mustUpdateHistoryWithEqualAtTheBeginning() {
        String expectedOutput = "=2\n";
        onView(withId(R.id.two_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.clear_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.history_screen)).check(matches(withText(expectedOutput)));
    }

    @Test
    public void test_clearHistoryBtn_shouldClearHistoryTextView() {
        onView(withId(R.id.two_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.result_btn)).perform(closeSoftKeyboard(), click());
        onView(withId(R.id.history_screen)).check(matches(not(withText(""))));

        onView(withId(R.id.history_clear_btn)).perform(click());
        onView(withId(R.id.history_screen)).check(matches(withText("")));
    }

    private void stubIntent() {
        Intent intent = new Intent();
        intent.setData(Uri.parse("content://com.android.contacts/data/1"));
        Instrumentation.ActivityResult result =
                new Instrumentation.ActivityResult(Activity.RESULT_OK, intent);
        intending(toPackage("com.android.contacts")).respondWith(result);
    }

    private String getPhoneNumber(Context targetContext2) {
        String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME};
        Cursor cursor =
                targetContext2.getContentResolver().query(Uri.parse("content://com.android.contacts/data/1"),
                        projection, null, null, null);

        cursor.moveToFirst();
        int numberColumnIndex =
                cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        return cursor.getString(numberColumnIndex);
    }
}