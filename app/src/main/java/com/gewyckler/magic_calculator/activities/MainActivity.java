package com.gewyckler.magic_calculator.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.magic_calculator.R;

import org.mariuszgromada.math.mxparser.Expression;

import java.util.Arrays;
import java.util.List;

/**
 * The idea for this application was taken from youtube channel "Mike Boyd": https://youtu.be/W89i7qqhuLQ
 * Know how for this project was taken from youtube channel "Practical Coding": https://youtu.be/B5b-7uDtUp4
 * <p>
 * Application is not a complete copy of the youtube one. There are some changes (way of handling brackets and method
 * that prevent user from typing the same mathematical symbols next to each other.
 * For example it is not possible to write 1++2 or 5-+4.
 * <p>
 * Application works like normal basic calculator.
 * To do magic trick user have to long touch equation button '='. Then choose contact from phone book.
 * Phone number of this contact from now will be a result of all equations.
 * To get back to normal calculator mode long touch clear button 'C'.
 */
public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private ScrollView scrollView;
    private TextView mHistory_screen;
    private EditText mResultTextView;
    private String phoneNumber = "";
    private final List<Character> MATH_SYMBOL_LIST = Arrays.asList('.', '÷', '×', '+', '-', '^');

    private ActivityResultLauncher<Intent> activityResultLauncher;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: called");
        super.onCreate(savedInstanceState);

        setTheme(R.style.Theme_Magic_calculator);
        setContentView(R.layout.main_layout);
        initLaunchers();
        initView();
    }

    private void initView() {
        Log.d(TAG, "initView: called");

        mHistory_screen = findViewById(R.id.history_screen);
        scrollView = findViewById(R.id.scroll_view);

        mResultTextView = findViewById(R.id.result_screen);
        mResultTextView.setShowSoftInputOnFocus(false);

        Button resultBtn = findViewById(R.id.result_btn);
        resultBtn.setOnLongClickListener(view -> {
            getPhoneNumber();
            return false;
        });

        Button clearBtn = findViewById(R.id.clear_btn);
        clearBtn.setOnLongClickListener(view -> {
            phoneNumber = "";
            clearBtn(view);
            return false;
        });

        ImageButton helpBtn = findViewById(R.id.help_btn);
        helpBtn.setOnClickListener(V -> {

            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setCancelable(true)
                    .show();

            View alertLayout = getLayoutInflater().inflate(R.layout.help_alert_dialog, null, false);
            alertDialog.setContentView(alertLayout);
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        });
    }

    private void initLaunchers() {
        Log.d(TAG, "initActivityResultLauncher: called");
        activityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), (ActivityResult result) -> {
            if (result.getResultCode() == RESULT_OK && result.getData() != null) {

                Intent intent = result.getData();
                Uri uri = intent.getData();
                Cursor cursor = getContentResolver().query(uri, null,
                        null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    phoneNumber = changePhoneNumberFormat(cursor.getString(numberIndex));
                }
            }
        });
    }

    private String changePhoneNumberFormat(String givenPhoneNumber) {
        phoneNumber = givenPhoneNumber.replaceAll("-", "").replaceAll(" ", "");
        return phoneNumber = givenPhoneNumber.replaceFirst("[+]..", "").replaceAll(" ", "");
    }


    private void getPhoneNumber() {
        Log.d(TAG, "getPhoneNumber: called");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, 0);
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
            intent = Intent.createChooser(intent, "Choose a phone number");
            if (intent.resolveActivity(getPackageManager()) != null) {
                activityResultLauncher.launch(intent);
            }
        }
    }

    private void appendToCursor(String textToAdd) {
        Log.d(TAG, "updateText: called");
        String oldString = mResultTextView.getText().toString();
        int cursorPos = mResultTextView.getSelectionStart();

        Log.d(TAG, "updateText: Result screen contains user input");
        String leftString = oldString.substring(0, cursorPos);
        String rightString = oldString.substring(cursorPos);
        mResultTextView.setText(leftString + textToAdd + rightString);
        mResultTextView.setSelection(++cursorPos);
    }

    private void validateEquation(String userInput) {
        Log.d(TAG, "checkLastCharacter: called");

        String currentText = mResultTextView.getText().toString();
        if (Character.isDigit(userInput.charAt(0))) {
            Log.d(TAG, "checkLastCharacter: User input is a digit");

            if (currentText.equals(getString(R.string.error_message)) || currentText.equals(getString(R.string.default_screen)) || currentText.equals(getString(R.string.default_screen_sec))) {
                mResultTextView.setText(userInput);
                mResultTextView.setSelection(mResultTextView.getText().length());
            } else {
                appendToCursor(userInput);
            }

        } else if (!Character.isDigit(userInput.charAt(0))) {
            Log.d(TAG, "checkLastCharacter: User input is NOT a digit");

            int cursorPosition = mResultTextView.getSelectionStart();
            char lastCharacterBeforeCursor = 0;
            if (cursorPosition > 0) {
                lastCharacterBeforeCursor = mResultTextView.getText().toString().charAt(cursorPosition - 1);
            }

            if (currentText.equals(getString(R.string.error_message))) {
                mResultTextView.setText(R.string.default_screen);

            } else if (currentText.equals(getString(R.string.default_screen)) || currentText.equals(getString(R.string.default_screen_sec))) {
                mResultTextView.setSelection(mResultTextView.getText().toString().length());
                appendToCursor(userInput);

            } else if (MATH_SYMBOL_LIST.contains(lastCharacterBeforeCursor)) {
                Log.d(TAG, "updateText: Last character at result screen and user input are mathematical symbols");
                changeCharacterBeforeCursor(userInput, cursorPosition);

            } else {
                appendToCursor(userInput);
            }
        }
    }

    private void changeCharacterBeforeCursor(String userInput, int cursorPos) {
        String resultText = mResultTextView.getText().toString();
        String leftString = resultText.substring(0, cursorPos - 1);
        String rightString = resultText.substring(cursorPos);
        mResultTextView.setText(leftString + userInput + rightString);
        mResultTextView.setSelection(mResultTextView.length());
    }

    private void updateHistory(String newText) {
        mHistory_screen.append(newText + "\n");
        scrollView.fullScroll(View.FOCUS_DOWN);
    }

    public void zeroBtn(View view) {
        validateEquation("0");
    }

    public void oneBtn(View view) {
        validateEquation("1");
    }

    public void twoBtn(View view) {
        validateEquation("2");
    }

    public void threeBtn(View view) {
        validateEquation("3");
    }

    public void fourBtn(View view) {
        validateEquation("4");
    }

    public void fiveBtn(View view) {
        validateEquation("5");
    }

    public void sixBtn(View view) {
        validateEquation("6");
    }

    public void sevenBtn(View view) {
        validateEquation("7");
    }

    public void eightBtn(View view) {
        validateEquation("8");
    }

    public void nineBtn(View view) {
        validateEquation("9");
    }

    public void sumBtn(View view) {
        validateEquation("+");
    }

    public void subtractBtn(View view) {
        validateEquation("-");
    }

    public void multiplyBtn(View view) {
        validateEquation("×");
    }

    public void divideBtn(View view) {
        validateEquation("÷");
    }

    public void commaBtn(View view) {
        validateEquation(".");
    }

    public void plusMinusBtn(View view) {
        validateEquation("-");
    }

    public void clearBtn(View view) {
        updateHistory(getString(R.string.result) + mResultTextView.getText().toString());
        mResultTextView.setText(R.string.default_screen);
    }

    public void openBracketsBtn(View view) {
        validateEquation("(");
    }

    public void closeBracketsBtn(View view) {
        validateEquation(")");
    }

    public void powerBtn(View view) {
        validateEquation("^");
    }

    public void resultBtn(View view) {
        Log.d(TAG, "resultBtn: called");
        String userExpression = mResultTextView.getText().toString();

        userExpression = userExpression.replaceAll("÷", "/");
        userExpression = userExpression.replaceAll("×", "*");

        Expression expression = new Expression(userExpression);
        String result = String.valueOf(expression.calculate());

        if (phoneNumber != null && phoneNumber.length() > 0) {
            Log.d(TAG, "resultBtn: result is a phone number");
            mResultTextView.setText(phoneNumber);
            mResultTextView.setSelection(phoneNumber.length());
        } else {
            Log.d(TAG, "resultBtn: normal calculator mode is used");
            mResultTextView.setText(result);
            mResultTextView.setSelection(result.length());
        }
        updateHistory(userExpression);
    }

    public void backBtn(View view) {
        Log.d(TAG, "backBtn: called");
        int cursorPos = mResultTextView.getSelectionStart();
        int textLength = mResultTextView.getText().length();

        if (cursorPos != 0 && textLength > 0) {
            Log.d(TAG, "backBtn: result screen is not empty, deleting last character");
            SpannableStringBuilder selection = (SpannableStringBuilder) mResultTextView.getText();
            selection.replace(cursorPos - 1, cursorPos, "");
            mResultTextView.setText(selection);
            mResultTextView.setSelection(cursorPos - 1);
            if (mResultTextView.getText().toString().isEmpty()) {
                Log.d(TAG, "backBtn: last character deleted, default value set");
                mResultTextView.setText(getString(R.string.default_screen));
            }
        }
    }

    public void clearHistory(View view) {
        Log.d(TAG, "clearHistory: called");
        mHistory_screen.setText("");
    }
}
